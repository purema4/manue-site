//dealing with zoom
(function(){
    // getting all sections
    let sections = document.querySelectorAll('section');
    // getting the header
    let header = document.querySelector('header');
    let allSections = [header, ...sections]
    
    //intersection observer options
    let options1 = {
        threshold: [0.1, 0.8]
    };


    function onShow(Elems) {
        //console.log(Elems)
        Elems.forEach((Elem) => {
            //if elem is on showed, apply zoom animation, if not reset
            if (Elem.intersectionRatio < 0.1) {
                Elem.target.classList.remove('animated-zoom');
            } else {
                Elem.target.classList.add('animated-zoom');
            }
        });
    }

    let observer = new IntersectionObserver(onShow, options1);

    //observing all sections + header
    for (let section of allSections) {
        observer.observe(section)
    }


    //interaction observer for desciption
    let descriptions = document.querySelectorAll('.description');

    function onShowDesc(descs) {
        //console.log(descs)
        descs.forEach((dec) => {
            if(dec.intersectionRatio > 0.3){
                dec.target.classList.add('animated');
            }
        });
    }

    let options2 = {
        threshold: [0.3]
    };

    let desObserver = new IntersectionObserver(onShowDesc, options2)

    for (let desc of descriptions) {
        desObserver.observe(desc);
    }
})()
