function initMap() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(pos){
            setMap({
                center: {lat: pos.coords.latitude, lng: pos.coords.longitude},
                zoom: 12
            });
        }, function(err) {

            setMap({
                center: {lat: 45.465044, lng: -75.6565847},
                zoom: 8
            });
        });
    } else {
        alert("Geolocation is not supported by this browser.");
        setMap({
            center: {lat: 45.465044, lng: -75.6565847},
            zoom: 8
        });
    }
}

function setMap(opt) {
    console.log(opt);
    var map = new google.maps.Map(document.getElementById('map'), opt);    
}
